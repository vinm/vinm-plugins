# Vīnm Plugins

> Official plugins for Vinm CLI.

🚸 Extend the [Vinm CLI](https://gitlab.com/vinm/vinm-cli)


## 🔌 Plugins list

- [vinm-plugin-utils](./vinm-plugin-utils/)
- [vinm-plugin-aws](./vinm-plugin-aws/)


## ⌨️ Writing plugins

A Plugin is custom Javascript code that creates new or extends existing commands within the Vinm CLI, while hooks binds code to any lifecycle event. Below is the list of currently supported hooks:

- **afterReadYmlConfig**: After reading the `vinm.yml` config file

To be valid, plugins need to export a `plugin()` function that return an object containing the differents hooks to use, and their associated async functions.

Sample plugin:

```javascript
module.exports.plugin = () => {
    const vinmHooks = {
        afterReadYmlConfig: async({ ymlConfig, pluginDirectory }) => {
            return Promise.resolve(ymlConfig)
        }
    }
    return vinmHooks
}
```


## 🤝 Contributing

Contributions, issues and feature requests are welcome.


## 📝 License

Copyright © 2020 [maoosi](https://gitlab.com/maoosi).<br />
This project is [MIT](./LICENSE) licensed.
