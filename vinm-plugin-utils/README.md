# Vīnm Plugin Utils

## 📦 Installation

Install package:

```sh
# With npm
npm i vinm-plugin-utils

# OR With yarn
yarn add vinm-plugin-utils
```

Then, add into your `vinm.yml` file:

```yml
plugins:
    - vinm-plugin-utils

tasks:
    mytask:
        shell: >-
            vinm@utils ... # see usage below
```

## 🚀 Usage

This plugin give access to the following helpers:

```sh
# Return your IP address
vinm@utils --exec "myip" --vinmVar "[vinmVar]"

# Merge given --data into .json --file 
vinm@utils --exec "mergeJson" --file "[file]" --data "[data]"

# Merge given --data into .env --file 
vinm@utils --exec "mergeEnv" --file "[file]" --data "[data]"

# Print given --data in the terminal
vinm@utils --exec "printf" --data "[data]"

# Run given node.js script --file and call --func using --* parameters
vinm@utils --exec "runScript" --file "[file]" --func "[func]" --* "[*]"
```
