const vinm = require('vinm-nodejs-emitter')()
const publicIp = require('public-ip')
const path = require('path')
const detectIndent = require('detect-indent')
const merge = require('lodash.merge')
const fs = require('fs')

const myip = async ({ vinmVar }) => {

    // read my IP address
    let myip = await publicIp.v4()

    // send it to Vinm
    vinm(vinmVar, myip)

}

const mergeJson = async ({ file, data }) => {

    // read input json file
    let jsonFile = fs.readFileSync(path.join(process.cwd(), file), 'utf8')
    let indent = detectIndent(jsonFile).indent || '    '
    let input = JSON.parse(jsonFile)

    // merge with data
    let mergeData = JSON.parse(data)
    let output = merge(input, Object.assign({}, mergeData))

    // update json file
    fs.writeFileSync(path.join(process.cwd(), file), JSON.stringify(output, null, indent))

}

const mergeEnv = async ({ file, data }) => {

    // read input json file
    let envFile = fs.readFileSync(path.join(process.cwd(), file), 'utf8')

    // merge with data
    data.split(',').forEach((replaceGroup) => {
        let [key, value] = replaceGroup.split('==')
        let regex = new RegExp(`${key}=(.*)`, 'gm')
        let matches = envFile.match(regex)

        if (matches !== null && matches.length > 0) {
            matches.forEach((m) => {
                envFile = envFile.replace(m, `${key}=${value}`)
            })
        }
    })

    // update env file
    fs.writeFileSync(path.join(process.cwd(), file), envFile)

}

const printf = async ({ data }) => {

    // init printf
    let printf = ''

    // merge with data
    data.split(',').forEach((dataGroup) => {
        let [key, value] = dataGroup.split('==')
        printf = printf.concat(`\n${key}=${value}`)
    })

    // output data
    console.log(printf)

}

const runScript = async (args) => {

    // execute node script
    await require(path.join(process.cwd(), args.file))[args.func](args)

}

module.exports.run = async (params) => {
    try {
        let func = params.exec
        delete params.exec
        await eval(`${func}(${JSON.stringify(params)})`)
        process.exit()
    } catch (err) {
        console.error(err)
        process.exit()
    }
}

module.exports.plugin = () => {

    const afterReadYmlConfig = async ({ ymlConfig, pluginDirectory }) => {
        for (let task in ymlConfig.tasks) {
            ymlConfig.tasks[task].shell =
                ymlConfig.tasks[task].shell.replace('vinm@utils', `node "${pluginDirectory}" run`)
        }
        return Promise.resolve(ymlConfig)
    }

    return {
        afterReadYmlConfig
    }

}

require('make-runnable/custom')({
    printOutputFrame: false
})