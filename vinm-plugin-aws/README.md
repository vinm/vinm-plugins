# Vīnm Plugin AWS

## 📦 Installation

Install package:

```sh
# With npm
npm i vinm-plugin-aws

# OR With yarn
yarn add vinm-plugin-aws
```

Then, add into your `vinm.yml` file:

```yml
plugins:
    - vinm-plugin-aws

tasks:
    mytask:
        shell: >-
            vinm@aws ... # see usage below
```

## 🚀 Usage

This plugin give access to the following helpers:

```sh
# AWS Secrets Manager:
#   generate and store randomly generated secrets for a given --id and --keys
vinm@aws --exec "secrets" --id "[id]" --keys "[key1,key2]" --profile "[profile]" --region "[region]"

# AWS Systems Manager Parameter Store:
#   store a list of given --params
vinm@aws --exec "parameters" --params "[params]" --profile "[profile]" --region "[region]"

# AWS CloudFormation:
#   read a given cloudformation output and store results in $vinm.[vinmVar]
vinm@aws --exec "cf" --stack "[stack]" --vinmVar "[vinmVar]" --profile "[profile]" --region "[region]"

# Amazon EC2 Key Pairs:
#   generate or retrieve keypair --alias given, save it into S3 --bucket, and save it locally into --save folder
vinm@aws --exec "keypair" --alias "[alias]" --bucket "[bucket]" --profile "[profile]" --region "[region]" --save "[save]"
```
