const vinm = require('vinm-nodejs-emitter')()
const AWS = require('aws-sdk')
const fs = require('fs-extra')
const path = require('path')

const secrets = async ({ id, keys, profile, region }) => {
    // instanciate
    let [passwords, requireUpdate] = [{}, false]
    let [secret, secretId, secretKeys, secretArn, secretValue] =
        [false, id, keys.split(','), false, {}]
    let secretsmanager = new AWS.SecretsManager({
        credentials: new AWS.SharedIniFileCredentials({ profile: profile }),
        region: region
    })

    // generate new secrets (or random passwords)
    for (const secretKeyParam of secretKeys) {
        const secretKeys = secretKeyParam.split(':')

        let randomPassword
        if (typeof secretKeys[1] !== 'undefined' && secretKeys[1] === 'dbUsername') {
            randomPassword = await secretsmanager.getRandomPassword({
                IncludeSpace: false,
                PasswordLength: 12,
                RequireEachIncludedType: false,
                ExcludePunctuation: true,
                ExcludeNumbers: true
            }).promise()
        } else {
            randomPassword = await secretsmanager.getRandomPassword({
                IncludeSpace: false,
                PasswordLength: 12,
                RequireEachIncludedType: false,
                ExcludePunctuation: true
            }).promise()
        }

        passwords[ secretKeys[0] ] = randomPassword.RandomPassword
    }

    // check if secrets already exist
    try {
        secret = await secretsmanager.getSecretValue({ SecretId: secretId }).promise()
    }
    catch (err) {}

    // secrets already exist
    if (secret) {
        secretValue = JSON.parse(secret.SecretString) || {}

        // require update?
        Object.keys(passwords).forEach((key) => {
            if (typeof secretValue[key] === 'undefined') {
                requireUpdate = true
                secretValue[key] = passwords[key]
            }
        })

        // update the secrets
        if (requireUpdate) {
            secret = await secretsmanager.updateSecret({
                SecretId: secretId,
                SecretString: JSON.stringify(secretValue)
            }).promise()
        }
    }
    // secrets doesn't exist, create new ones
    else {
        secret = await secretsmanager.createSecret({
            Name: secretId,
            SecretString: JSON.stringify(passwords)
        }).promise()
    }

    // send secrets to vinm
    vinm('secrets', {
        arn: secret.ARN,
        value: secretValue
    })
}

const cf = async ({ stack, vinmVar, profile, region }) => {
    // instanciate
    let outputs = []
    let cfmanager = new AWS.CloudFormation({
        credentials: new AWS.SharedIniFileCredentials({ profile: profile }),
        region: region
    })

    // describe stack
    let output = await cfmanager.describeStacks({ StackName: stack }).promise()

    // parse outputs
    output.Stacks[0].Outputs.map(({ OutputKey, OutputValue }) => {
        if (typeof OutputKey !== 'undefined') outputs[OutputKey] = OutputValue
    })

    // send it to Vinm
    vinm(vinmVar, outputs)
}

const keypair = async ({ alias, bucket, profile, region, save = false }) => {
    // sdk config
    AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: profile })
    AWS.config.update({ region: region })

    // instanciate
    let [bucketExists, keypairExists, keypair, keypairBody] = [true, true, false, false]
    let [s3, ec2] = [new AWS.S3(), new AWS.EC2()]

    // check if keypair exists
    try { keypair = await ec2.describeKeyPairs({ KeyNames: [alias] }).promise() }
    catch (err) { keypairExists = false }

    // keypair doesn't exist
    if (!keypairExists) {
        // create new keypair
        keypair = await ec2.createKeyPair({
            KeyName: alias
        }).promise()

        // check if bucket exists
        try { await s3.headBucket({ Bucket: bucket }).promise() }
        catch (err) { bucketExists = false }

        // create new bucket
        if (!bucketExists) {
            await s3.createBucket({
                Bucket: bucket,
                CreateBucketConfiguration: {
                    LocationConstraint: region
                }
            }).promise()
        }

        // store keypair on s3
        keypairBody = keypair.KeyMaterial
        await s3.putObject({ Body: keypairBody, Bucket: bucket, Key: `${alias}.pem` }).promise()
    } else {
        // read keypair
        keypair = await s3.getObject({ Bucket: bucket, Key: `${alias}.pem` }).promise()
        keypairBody = keypair.Body.toString()
    }

    // log
    console.log(`Keypair "${alias}" can be downloaded from the S3 bucket "${bucket}".`)

    // store the key and send path to vinm
    if (save) {
        let sshKeyFile = path.join(save, `${alias}.pem`)
        let exists = await fs.pathExists(sshKeyFile)
        if (exists) await fs.chmod(sshKeyFile, 0o755)
        await fs.outputFile(sshKeyFile, keypairBody)
        await fs.chmod(sshKeyFile, 0o400)
        vinm('sshKey', path.join(process.cwd(), sshKeyFile))
    }
}

const parameters = async ({ params, profile, region }) => {
    // sdk config
    AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: profile })
    AWS.config.update({ region: region })

    // instanciate
    let [ssm] = [new AWS.SSM()]

    // extract params
    for (const paramsGroup of params.split(',')) {
        let [name, value] = paramsGroup.split('==')

        // store parameters
        await ssm.putParameter({
            Name: name,
            Type: 'String',
            Value: value,
            Overwrite: true
        }).promise()
    }
}

module.exports.run = async (params) => {
    try {
        let func = params.exec
        delete params.exec
        await eval(`${func}(${JSON.stringify(params)})`)
        process.exit()
    } catch (err) {
        console.error(err)
        process.exit()
    }
}

module.exports.plugin = () => {

    const afterReadYmlConfig = async ({ ymlConfig, pluginDirectory }) => {
        for (let task in ymlConfig.tasks) {
            ymlConfig.tasks[task].shell =
                ymlConfig.tasks[task].shell.replace('vinm@aws', `node "${pluginDirectory}" run`)
        }
        return Promise.resolve(ymlConfig)
    }

    return {
        afterReadYmlConfig
    }

}

require('make-runnable/custom')({
    printOutputFrame: false
})