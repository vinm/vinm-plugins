# Changelog

All notable changes to this project will be documented in this file.


## [2.1.0]

- vinm@aws --exec "secrets" now returns both the secret arn and secret value


## [2.0.0]

- Various dependencies upgraded to latest versions
- Semantic Versioning updated to align with Vinm CLI and Vinm Node.js emitter v2.0.0